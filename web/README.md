# Front-end
This part of the project implements the user interface of the application and uses the API to persist data.

# Requirements
To be able to run this part of the project you'll need to have the following requirements met:

* Node.js and npm

# Quickstart

Here is a quick start to get everything running, you should be able just to copy and paste the following commands and have everything set up.

## Running the application

```
git clone git@bitbucket.org:pxotox/ck-code-challenge-python.git
cd ck-code-challenge-python/web/
npm install
npm run serve
```

## Acessing the application

After running the application you'll be able to access it here: [http://localhost:8080/](http://localhost:8080/).

## Running tests

To run tests you can execute this command: `npm run test`.
