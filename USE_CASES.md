# Use cases

Here are all use cases to be implemented on our application.

## 1. Completing a task (UC001)
This use case adds the feature of flagging a task as completed when the user clicks on the checkbox besides it.

* **Use case**: As an user I want to complete tasks so that I can see the ones tha are left
* **Acceptance criteria #1**: Given that I have already some tasks added to the list when I do check or uncheck checkbox I expect that it reflects the status on the database

![UC001 Mockup](https://i.ibb.co/0M3rMfz/UC001.png)

## 2. Editing a task (UC002)
This use case adds the feature of editing the title an existing task.

* **Use case**: As an user I want to edit existing tasks so that I can change it's title
* **Acceptance criteria #1**: Given that I have already some tasks added to the list when I do double click on them I expect that an input shows up so I can edit the title
* **Acceptance criteria #2**: Given I am already editing an task when I do press enter I expect that the title of the task is updated on the screen and on the database
* **Acceptance criteria #3**: Given I am already editing an task when I do press esc I expect that the edition is cancelled and the item goes back to its normal state

![UC002 Mockup](https://i.ibb.co/xmprNbW/UC002.png)

## 3. Removing a task (UC003)
This use case adds the feature of removing an existing task.

* **Use case**: As an user I want to remove existing tasks so that I can clean the list
* **Acceptance criteria #1**: Given that I have already some tasks added to the list when I do click on the remove icon I expect that the task is removed from the list and persisted on the database

**Note:** no confirmation is needed

![UC003 Mockup](https://i.ibb.co/Fg6f4gT/UC003.png)

## 4. Bookmarking a task (UC004)
This use case adds the feature of bookmarking a task and making them visible on the top of the list.

* **Use case**: As an user I want to bookmark existing tasks so that I can see them at the top of the list
* **Acceptance criteria #1**: Given that I have already some tasks added to the list when I do click on the bookmark icon I expect that the task is moved to the top of the list and stays there if I reload the page

![UC004 Mockup](https://i.ibb.co/DfWTMCD/UC004.png)

## 5. Filtering tasks (UC005)
This use case adds the feature of filtering tasks by All (no filter), Active (only not completed tasks), Completed (only completed tasks).

* **Use case**: As an user I want to filter existing tasks so that I can see only all, only active or only completed tasks
* **Acceptance criteria #1**: Given that I have already some tasks added to the list when I do click on the different filtering options I expect that the filter behaves as expected

![UC005 Mockup](https://i.ibb.co/vmVgy5S/UC005.png)

# Extra use cases

The following use cases can be implemented if you want to go the extra mile and show your talent.

## 6. Visual identification of completed tasks (UC006)
This use case adds visual identification to completed tasks.

* **Use case**: As an user I want to see a clear identification of completed tasks so that I can visually identify them
* **Acceptance criteria #1**: Given that I enter the app when I do list or add a new task I expect that I can see a visual identification of items that are completed

![UC006 Mockup](https://i.ibb.co/By4j2vc/UC006.png)

Use this example checkbox to implement it: [Material Design Checkbox by Andreas Storm](https://codepen.io/andreasstorm/pen/deRvMy).

## 7. Recommendation system (UC007)
This use case adds a feature to recommend products from Walmart for tasks that start with the word "buy". It should be done only for new tasks and not for tasks loaded from the backend.

* **Use case**: As an user I want get suggestions where to buy items from the list so that I can directly go to shopping sites
* **Acceptance criteria #1**: Given that I am adding a new task when I do write "buy" at the begginning of the title I expect that I get recommendations of where to buy the item on the list

**Note**: Highlight as bold all words of the search when displaying it to the user

To implement this you can use [Walmart's Search API](https://developer.walmartlabs.com/docs/read/Search_API):

* API Key: `yk8xqdrcs8sbgatjrvjt9ehc`
* Search API (example): `http://api.walmartlabs.com/v1/search?apiKey=yk8xqdrcs8sbgatjrvjt9ehc&query=milk`

![UC007 Mockup](https://i.ibb.co/LJ4LhgR/UC007.png)
