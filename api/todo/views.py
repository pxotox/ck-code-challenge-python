"""
Module that implements all views
"""
import json

from django.core import serializers
from django.forms.models import model_to_dict
from django.http import JsonResponse
from django.shortcuts import get_object_or_404, render
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import View

from todo.models import Task


@method_decorator(csrf_exempt, name='dispatch')
class TasksView(View):
    """
    Implements /api/v1/tasks
    """
    def get(self, request):
        """
        Return list of all tasks
        """
        return JsonResponse({
            'result': [i.to_dict() for i in Task.objects.all()]
        })

    def post(self, request):
        """
        Creates a new task
        """
        body = json.loads(request.body.decode('utf-8'))
        task = Task.objects.create(title=body['title'])
        return JsonResponse(task.to_dict())


@method_decorator(csrf_exempt, name='dispatch')
class TaskDetailsView(View):
    """
    Implements /api/v1/tasks/:id
    """
    def get(self, request, id):
        """
        Return a specific Task object
        """
        task = get_object_or_404(Task, pk=id)
        return JsonResponse(task.to_dict())

    def put(self, request, id):
        """
        Updates a specific Task
        """
        task = get_object_or_404(Task, pk=id)
        body = json.loads(request.body.decode('utf-8'))
        task.title = body['title']
        task.completed = body['completed']
        task.save()
        return JsonResponse(task.to_dict())
