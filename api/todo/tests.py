"""
Module that implements all unit tests
"""
import datetime
import json
import random

import pytz
from django.test import Client, TestCase
from django.urls import reverse
from django.utils import timezone

from mock import patch
from todo.models import Task


class ApiTestCase(TestCase):
    """
    Tests all API endpoints
    """
    def setUp(self):
        """
        Creates new data to be used by each test
        """
        self.title = 'Test task {}'.format(random.randint(1, 9999))
        self.hour = random.randint(1, 23)
        self.date = datetime.datetime(2018, 12, 21, self.hour, 55, 33, 0, tzinfo=pytz.UTC)

    def get_expected_json_result(self):
        """
        Return the expected JSON object from the API
        """
        return {
            'id': 1,
            'title': self.title,
            'completed': False,
            'created_at': self.date.isoformat(),
            'updated_at': self.date.isoformat()
        }

    def test_list_all_tasks(self):
        """
        Test retrieving all tasks (GET api/v1/tasks)
        """
        with patch.object(timezone, 'now', return_value=self.date) as _:
            Task.objects.create(title=self.title)
            response = self.client.get(reverse('v1_tasks'))
            self.assertEqual(response.status_code, 200)
            self.assertJSONEqual(
                str(response.content, encoding='utf8'),
                {'result': [self.get_expected_json_result()]}
            )

    def test_create_task(self):
        """
        Test creating a new task (POST api/v1/tasks)
        """
        with patch.object(timezone, 'now', return_value=self.date) as _:
            test_object = {'title': self.title}
            response = self.client.post(reverse('v1_tasks'),
                                        json.dumps(test_object),
                                        content_type='application/json')
            self.assertEqual(response.status_code, 200)
            self.assertJSONEqual(
                str(response.content, encoding='utf8'),
                self.get_expected_json_result()
            )

    def test_get_task(self):
        """
        Test retrieving an existing task (GET api/v1/tasks/:id)
        """
        with patch.object(timezone, 'now', return_value=self.date) as _:
            task = Task.objects.create(title=self.title)
            response = self.client.get(reverse('v1_tasks_object',
                                               kwargs={'id': task.id}))
            self.assertEqual(response.status_code, 200)
            self.assertJSONEqual(
                str(response.content, encoding='utf8'),
                self.get_expected_json_result()
            )

    def test_update_task(self):
        """
        Test updating an existing task (PUT api/v1/tasks/:id)
        """
        with patch.object(timezone, 'now', return_value=self.date) as _:
            task = Task.objects.create(title=self.title)
            data = {
                'title': 'Test task {}'.format(random.randint(-100, -1)),
                'completed': not task.completed
            }
            response = self.client.put(reverse('v1_tasks_object', kwargs={'id': task.id}),
                                       json.dumps(data),
                                       content_type='application/json')
            self.assertEqual(response.status_code, 200)
            expected = self.get_expected_json_result()
            expected['title'] = data['title']
            expected['completed'] = data['completed']
            self.assertJSONEqual(
                str(response.content, encoding='utf8'),
                expected
            )
