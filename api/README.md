# API (Back-end)
This part of the project implements the REST API used by the front-end to retrieve and save data.

# Requirements
To be able to run this part of the project you'll need to have the following requirements met:

* Python 3.6+
* Pip

# Quickstart

Here is a quick start to get everything running, you should be able just to copy and paste the following commands and have everything set up.

## Running the application

```
git clone git@bitbucket.org:pxotox/ck-code-challenge-python.git
cd ck-code-challenge-python/api/
pip install -r requirements
python manage.py migrate
python manage.py run 0.0.0.0:8000
```

## Acessing the application

After running the application you'll be able to access it here: [http://localhost:8000/api/v1/tasks](http://localhost:8000/api/v1/tasks).

## Running tests

To run Django's unit tests you can execute this command: `python manage.py test`.

## Adding migrations

If you edit any model (inside `todo/models.py`) and need to update or create new migrations, you can execute the following commands: 

* Create missing migrations: `python manage.py makemigrations`
* Execute new migrations: `python manage.py migrate`
