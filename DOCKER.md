# Quickstart with Docker
If you choose to implement this code challenge using docker, good for you! You should be able just to copy and paste the following commands and have everything set up.

## Running the application

```
git clone git@bitbucket.org:pxotox/ck-code-challenge-python.git
cd ck-code-challenge-python/
docker-compose up
```

## Acessing the application

After following the quickstart you'll be able to access the application through these urls:

* http://localhost:8080/ - Single-Page application
* http://localhost:8000/api/v1/tasks - Tasks REST API

## Running the tests

To run the tests you can execute these commands:

* Vue.js unit tests: `docker-compose exec web npm run test`
* Django unit tests: `docker-compose exec api python manage.py test`

## Adding migrations

If you edit any model and need to update the migrations, you can execute the following commands:

* Create missing migrations: `docker-compose exec api python manage.py makemigrations`
* Execute new migrations: `docker-compose exec api python manage.py migrate`

## Executing commands on a running container

If you need to execute any bash command in the container, use the following commands:

* Vue.js container: `docker-compose exec web [COMMAND]`
* Django container: `docker-compose exec api [COMMAND]`

If you want to connect to the container you can execute these commands:

* Vue.js container: `docker-compose exec web bash`
* Django container: `docker-compose exec api bash`
